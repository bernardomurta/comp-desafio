import React from 'react'
import { StyleSheet } from 'react-native'
import Header from './Header'
import Card from './card'

export default function App() {
  return (
    <>
      <Header />
      <Card cor="white" texto="Saber mais" />
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
